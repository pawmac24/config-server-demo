### Running and compiling from scripts:
- `./run_config_server.sh` 
- `./run_config_server.cmd`

The config server runs for given profile:
http://localhost:8888/holiday-service-api/{spring-profile}

###List of given profiles
<pre>
{profile}:
  local (default),
  dev,
  prod,
  test - running test
</pre>
